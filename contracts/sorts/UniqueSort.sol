pragma solidity >=0.4.18 <0.6.0;

contract UniqueSort {

    function unique(uint[] memory data, uint setSize) internal pure {
        uint length = data.length;
        bool[] memory set = new bool[](setSize);
        for (uint i = 0; i < length; i++) {
            set[data[i]] = true;
        }
        uint n = 0;
        for (uint i = 0; i < setSize; i++) {
            if (set[i]) {
                data[n] = i;
                if (++n >= length) break;
            }
        }
    }
    
}
